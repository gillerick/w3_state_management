import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:w3_state_management/stacks.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:w3_state_management/main.dart';
import 'package:w3_state_management/splash_screen.dart';

// void main() => runApp(MyApp());

final List<String> _stacks = [
  'Adonis',
  'Go',
  'Quarkus',
  'MySQL',
  'Vue JS',
  'Flutter'
];

/// This is the stateful widget that the main application instantiates.
class Home extends StatelessWidget {

  String _item = _stacks[1];
  String _selected_item = _stacks[1];

  @override
  Widget build(BuildContext context) {
    String appTitle = context.read<DataHolder>().appName;
    String logoPath = context.read<DataHolder>().logoPath;

    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(logoPath,
              fit: BoxFit.contain,
              height: 32,
            ),
            Container(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 0), child: Text(appTitle))
          ],

        ),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              "Welcome to $appTitle",
              style: TextStyle(
                height: 5,
                fontSize: 26,
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(13),
              child: TextField(
                onChanged: (val) => context.read<DataHolder>().change(userName: val),
                decoration: InputDecoration(
                    border: OutlineInputBorder(), hintText: "Enter your name"),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(13.0),
              child: DropdownButtonFormField(
                decoration: const InputDecoration(
                  labelText: 'Pick a stack',
                ),
                value: _selected_item,
                items: _stacks
                    .map((label) => DropdownMenuItem(
                          child: Text(label),
                          value: label,
                        ))
                    .toList(),
                onChanged: (val) => context.read<DataHolder>().change(stack: val)),
              ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 200, 0, 0),
              child: Body(),
            ),
          ],
        ),
      ),
    );
  }
}

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String input;
    return Align(
      alignment: Alignment.bottomCenter,
      child: FloatingActionButton.extended(
        onPressed: () => {
          // input = nameController.text,
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => Stacks()),
          ),


        },
        label: Text("Register"),
        icon: const Icon(Icons.add),
      ),
    );
  }
}
