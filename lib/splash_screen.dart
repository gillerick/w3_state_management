import 'dart:async';
// import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:w3_state_management/home.dart';
import 'package:w3_state_management/main.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 6),
            () => Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => Home())));
  }

  @override
  Widget build(BuildContext context) {
    String appTitle = context.read<DataHolder>().appName;
    String logoPath = context.read<DataHolder>().logoPath;

    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 90, 0, 20),
              child: Image.asset(logoPath, height: 160,),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Text(appTitle, style: TextStyle(fontSize: 15, fontWeight: FontWeight.normal,),),
            ),
          ],
        ),
      ),
    );
  }
}
