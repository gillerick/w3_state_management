import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:w3_state_management/splash_screen.dart';
import 'package:flutter/services.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext contextP) {
    return MultiProvider(providers: [ChangeNotifierProvider(create: (_) => DataHolder())],
      child:  MaterialApp(
          debugShowCheckedModeBanner: false,
          home: SplashScreen()
      ),
    );
  }
}


class DataHolder with ChangeNotifier, DiagnosticableTreeMixin{
  String _appName = 'Provider';
  String _logoPath = 'assets/w3_logo.png';
  String _userName = "";
  String _stack = "";

  //Getters
  String get appName => _appName;
  String get logoPath => _logoPath;
  String get username => _userName;
  String get stack => _stack;

  void change({String appName, String logoPath, String userName, String stack}) {
    _appName = appName;
    _logoPath = logoPath;
    _userName = userName;
    _stack = stack;
    notifyListeners();
  }
}